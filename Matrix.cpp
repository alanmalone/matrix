//============================================================================
// Name        : Matrix.cpp
// Author      : AlanMalone
// Version     :
// Copyright   : Open Source
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "Matrix.h"
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <string>
#include <string.h>
#include <stdio.h>

using namespace std;

int Matrix::counter = 0;

void Matrix::init(int row, int col, const double* elements) {
    number = ++counter;
    if (row < 0 || col < 0) throw Denied(1, number);
    if (row == 0 || col == 0) {
        n=m=size=0;
        matrix = NULL;
    } else {
        n = col;
        m = row;
        size = m*n;
        matrix = new double [size];
        if (elements != NULL) {
            for (int i = 0; i < size; i++) matrix[i] = elements[i];
        } else {
            for (int i = 0; i < size; i++) matrix[i] = 0;
        }
    }
}

Matrix::Matrix(int row, int col, const double* elements) {
	init(row, col, elements);
	cout << "\nCreate matrix number " << number << endl;
}

Matrix::Matrix(int square, const double* elements) {
    init(square, square, elements);
    cout << "\nCreate matrix number " << number << endl;
}

Matrix::Matrix(int row, int col, double a, ...) {
    init(row, col, &a);
    cout << "\nCreate matrix number " << number << endl;
}

Matrix::Matrix(int square, double a, ...) {
    init(square, square, &a);
    cout << "\nCreate matrix number " << number << endl;
}

Matrix::Matrix(const Matrix& copy) {
	init(copy.m, copy.n, copy.matrix);
	cout << "Create matrix number " << number << " with copy constructor" << endl;
}

Matrix::~Matrix() {
	if (matrix != NULL) {
		delete [] matrix;
	}
	cout << "Delete matrix number: " << number << endl;
}

int Matrix::getSize() const {
    return size;
}

int Matrix::getRows() const {
    return m;
}

int Matrix::getColum() const {
    return n;
}

int Matrix::getNumber() const {
    return number;
}

bool Matrix::MultVerify(const Matrix& multiplier) const {
	return (n == multiplier.m);
}

bool Matrix::SumVerify(const Matrix& summand) const {
	return((n == summand.n) && (m == summand.m)); //Ошибка
}

double Matrix::MaxElement() const {
    if (matrix == NULL) {
        throw Denied(3, number);
    } else {
        double maxi = matrix[0];
        for (int i = 1; i < size; i++) {
            if (matrix[i] > maxi) {
                maxi = matrix[i];
            }
        }
        return maxi;
    }
}

double Matrix::MinElement() const {
    if (matrix == NULL) {
        throw Denied(3, number);
    } else {
        double min1 = matrix[0];
        for (int i = 1; i < size; i++) {
            if (matrix[i] < min1) {
                min1 = matrix[i];
            }
        }
        return min1;
    }
}

double* Matrix::operator[](int idx) {
    if (idx < 0 || idx >= m) { //Проверить верх
        throw Denied(2, number);
    } else {
        return matrix+idx*n;
    }
}

const double* Matrix::operator[](int idx) const {
    if (idx < 0 || idx >= m) {
        throw Denied(2, number);
    } else {
        return &matrix[idx*n];
    }
}

Matrix& Matrix::operator = (const Matrix &rhs) {
	if (size != rhs.size) {
        if (matrix != NULL) {
            delete [] matrix;
        }
		matrix = rhs.size > 0 ? new double [rhs.size] : NULL;
	}
	n = rhs.n;
	m = rhs.m;
	size = rhs.size;
	if (matrix != NULL) {
        for (int i=0; i<size; i++) {
            matrix[i] = rhs.matrix[i];
        }
	}
	return *this;
}

int Matrix::length() const {
	static char str[30];
	int max=0;
	ios_base::fmtflags ff;
	ff = cout.flags();
	for (int i = 0; i<size; i++) {
        if ((ff & ios_base::scientific) != 0) {
            sprintf(str, "%e", matrix[i]);
        } else
        if (ff & ios_base::fixed) {
            sprintf(str, "%f", matrix[i]);
        }
        else {
            sprintf(str, "%g", matrix[i]);
        }
        if ((int)strlen(str) > max) {
            max = strlen(str);
        }
	}
	return max+1;
}

ostream& operator << (ostream &os, const Matrix& out) {
	if (out.matrix) {
        int width = (int)os.width();
        if (width == 0) width = out.length();
		for (int i=0; i < out.m; i++) {
			for (int j=0; j < out.n; j++) {
				os << setw(width) << *(out.matrix + i*out.n + j);
			}
			os << endl;
		}
	} else {
		os << "Matrix is empty";
	}
	return os;
}

Matrix& Matrix::operator+=(const Matrix& rhs) {
	if (SumVerify(rhs)) {
		for (int i = 0; i < size; i++) {
			matrix[i] += *(rhs.matrix + i);
		}
	} else {
		throw Denied(4, number, rhs.number);
	}
	return *this;
}

Matrix& Matrix::operator-=(const Matrix& rhs) {
	if (SumVerify(rhs)) {
		for (int i = 0; i < size; i++) {
            *(matrix + i) -= *(rhs.matrix + i);
		}
	} else {
		throw Denied(4, number, rhs.number);
	}
	return *this;
}

Matrix& Matrix::operator*=(const Matrix& rhs) {
	if (MultVerify(rhs)) {
        Matrix result(n, rhs.m);
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				for (int k = 0; k < m; k++) {
					matrix[i*m + j] += *(matrix + i*n + k) * *(rhs.matrix + k*n + j);
				}
			}
		}
		*this = result;
	}
	else {
		throw Denied(5, number, rhs.number);
	}
	return *this;
}

Matrix& Matrix::operator*=(double value) {
	for (int i = 0; i < size; i++) {
        *(matrix + i) *= value;
	}
	return *this;
}

Matrix operator+(const Matrix& lhs, const Matrix& rhs) {
    if (lhs.SumVerify(rhs)) {
        Matrix result(lhs);
        result += rhs;
        return result;
    } else {
        throw Denied(4, lhs.getNumber(), rhs.getNumber());
    }
}

Matrix operator-(const Matrix& lhs, const Matrix& rhs) {
    if (lhs.SumVerify(rhs)) {
        Matrix result(lhs);
        result-=rhs;
        return result;
    } else {
        throw Denied(4, lhs.getNumber(), rhs.getNumber());
    }
}

Matrix operator*(const Matrix& lhs, const Matrix& rhs) {
    if (lhs.SumVerify(rhs)) {
        Matrix result(lhs);
        result*=rhs;
        return result;
    } else {
        throw Denied(5, lhs.getNumber(), rhs.getNumber());
    }
}

Matrix operator*(const Matrix& lhs, double value) {
    Matrix result(lhs);
    result *= lhs;
    return result;
}

Matrix operator*(double value, const Matrix& rhs) {
    Matrix result(rhs);
    result *= rhs;
    return result;
}

Denied::Denied(int _code, int _Matrix1, int _Matrix2) {
	code = _code;
	Matrix1 = _Matrix1;
	Matrix2 = _Matrix2;
	switch (code) {
	case 1:
		sprintf(message, "%s %d %s\n", "Incorratly size in Matrix", Matrix1, " constructor ");
		break;
	case 2:
		sprintf(message, "%s %d\n", "Incorratly index of Matrix", Matrix1);
		break;
    case 3:
        sprintf(message, "%s %d\n", "Not found elements in Matrix", Matrix1);
        break;
    case 4:
        sprintf(message, "%s %d %s %d\n", "Size of Matrix", Matrix1, "not equal Matrix", Matrix2);
        break;
    case 5:
        sprintf(message, "%s %d %s %d\n", "Number of colums in Matrix", Matrix1, "not equal number or rows in Matrix", Matrix2);
        break;
	}
}

int Denied :: get_code() const {
	return code;
}

const char* Denied::get_string() const {
	return message;
}
