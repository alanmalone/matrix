/*
 * Matrix.h
 *
 *  Created on: 19 ���. 2015 �.
 *      Author: script
 */

#ifndef MATRIX_H_
#define MATRIX_H_

#include <iostream>

using namespace std;

class Matrix {
private:
	int m;
	int n;
	double* matrix;
	int size;
	static int counter;
	int number;
	void init(int = 0, int = 0, const double * = 0);
public:
	Matrix(int, int, const double* = NULL);
	explicit Matrix(int = 0, const double* = NULL);
	Matrix(const Matrix&);
	Matrix(int, int, double, ...);
	Matrix(int, double, ...);
	~Matrix();
	int getSize() const;
	int getRows() const;
	int getColum() const;
	int getNumber() const; //�������������
	bool MultVerify(const Matrix&) const;
	bool SumVerify(const Matrix&) const;
	double MaxElement() const;
	double MinElement() const;
	double *operator[] (int);
	const double *operator[] (int) const;
	Matrix& operator = (const Matrix&);
	int length() const;//�������������
	friend ostream &operator << (ostream &os, const Matrix&);
	Matrix& operator+=(const Matrix&);
	Matrix& operator-=(const Matrix&);
	Matrix& operator*=(const Matrix&);
	Matrix& operator*=(double);
};

Matrix operator+(const Matrix& lhs, const Matrix& rhs);
Matrix operator-(const Matrix& lhs, const Matrix& rhs);
Matrix operator*(const Matrix& lhs, const Matrix& rhs);
Matrix operator*(const Matrix& lhs, double value);
Matrix operator*(double value, const Matrix& rhs);

class Denied {
private:
	int code;
	int Matrix1, Matrix2;
	char message[128];
public:
	Denied(int _code, int _Matrix1 = 0, int _Matrix2 = 0);
	int get_code() const;
	const char* get_string() const;
	int get_Matrix1() const;
	int get_Matrix2() const;
};
//ostream &ostream::operator << (const Matrix&);
#endif /* MATRIX_H_ */
